fun moneySpentBy(customer: Customer): Double =
        customer.orders.flatMap(Order::products).sumOf{it.price}