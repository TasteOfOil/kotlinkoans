data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {
    override operator fun compareTo(date:MyDate):Int{
        if (this.year > date.year) return 1
        if (this.year < date.year) return -1
        if (this.month > date.month) return 1
        if (this.month < date.month) return -1
        if (this.dayOfMonth > date.dayOfMonth) return 1
        if (this.dayOfMonth < date.dayOfMonth) return -1
        return 0
}
}

fun test(date1: MyDate, date2: MyDate) {
    // this code should compile:
    println(date1 < date2)
}